## Suicide Research

How many people die from suicide (per country over time)?

## Background
* The World Health Organisation (WHO) estimates that each year approximately one million people die from suicide, which represents a global mortality rate of 16 people per 100,000 or one death every 40 seconds. It is predicted that by 2020 the rate of death will increase to one every 20 seconds.
* In the last 45 years suicide rates have increased by 60% worldwide. Suicide is now among the three leading causes of death among those aged 15-44 (male and female). Suicide attempts are up to 20 times more frequent than completed suicides.

## Outcome
* I picked the Top 5 countries that have the most number of suicidal from 2015-2018.* 
![](https://i.imgur.com/2iBv800.png)


## Source
* https://en.wikipedia.org/wiki/List_of_countries_by_suicide_rate#List_by_other_sources_and_years_(1985%E2%80%932018)
* https://www.usnews.com/news/best-countries/slideshows/countries-with-the-highest-suicide-rates?slide=7
* http://apps.who.int/gho/data/view.main.MHSUICIDEv?lang=en