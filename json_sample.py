import csv, json

csvFilePath = 'Suicide rate per Country.csv'
jsonFilePath = 'Suicide rate per Country.json'


#Read the CSV and add the data dot a dictionary.
data = {}
with open(csvFilePath) as csvFile:
    csvReader = csv.DictReader(csvFile)
    for csvCol in csvReader:
        country = csvCol['Country']
        data[country] = csvCol

#Write data to JSON file.
with open(jsonFilePath, 'w') as jsonFile:
    jsonFile.write(json.dumps(data, indent=4))
