pip install urllib
pip install xml
from xml.etree import ElementTree
from urllib.request import urlopen
import csv
import sys

# Read the Wikipedia page and parse it using etree
page = urlopen('https://en.wikipedia.org/wiki/List_of_countries_by_suicide_rate\
#List_by_other_sources_and_years_%281985%E2%80%932018%29').read()
tree = ElementTree.fromstring(page)

# Point on right table, rows and cells
table = tree.findall('.//table')[4]

# form header row and open csv file for writting
header_row = ['Regions', 'Male', 'Female', 'Average', 'Year']
with open('wiki.csv', 'w') as csv_file:
    writer = csv.writer(csv_file, delimiter=',', lineterminator='\n')
    writer.writerow(header_row)
    # loop through table body rows    
    for row in table.find('tbody').findall('tr'):
        cells = row.findall('td')
        try:
            # form row to write in csv file as a list, and clear unwanted end of lines
            # split and clear fourth column (data is in %)
            csv_row = [cells[0].find('.//a').attrib.get('title').replace('\n', ''),
                       cells[1].text.replace('\n', ''),
                       cells[2].text.replace('\n', ''),
                       cells[3].text.replace('\n', '').split('(')[1].split('%')[0],
                       cells[4].text.replace('\n', '')]
            writer.writerow(csv_row)
        except:
            pass